#!/bin/bash
sudo mkdir ~/flask-server
sudo cp -R ./* ~/flask-server/
sudo cp flask.service /etc/systemd/system/
sudo chown ubuntu ~/flask-server/start.sh
sudo chmod +x ~/flask-server/start.sh
sudo systemctl daemon-reload
sudo systemctl enable --now flask
#sudo systemctl status flask